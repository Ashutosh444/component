#!/usr/bin/env groovy

def checkoutCode() {
    stage("Checking out code repository") {
        checkout scm
    }
}

def readPropertyFile(Map stepParams) {
    config = readProperties file: "${stepParams.configFilePath}"
    return config
}

def containerRun(Map stepParams) {
    sh "docker  run ${stepParams.imageName} ${stepParams.url} ${stepParams.string}"
 }

def call(Map stepParams) {
    def config = [:]
    checkoutCode()
    config = commonUtils.readPropertyFile(
        configFilePath: "${stepParams.configFilePath}",
    )

    containerRun(
        imageName: "${config.IMAGE_NAME}",
        url: "${config.URL}",
        string: "${config.STRING}",
    )

}

